# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

movie = Movie.create(
    name: 'Citizen Kane',
    ranking: 1
)
movie.poster.attach(
    io: File.open(Rails.root.join('app', 'assets', 'images', 'citizen_kane.jpg')),
    filename: 'citizen_kane.jpg'
)

movie = Movie.create(
    name: 'Once Upon a Time... In Hollywood',
    ranking: 2
)
movie.poster.attach(
    io: File.open(Rails.root.join('app', 'assets', 'images', 'once_upon_a_time_in_hollywood.jpg')),
    filename: 'once_upon_a_time_in_hollywood.jpg'
)

movie = Movie.create(
    name: 'Rise of the Planet of the Apes',
    ranking: 3
)
movie.poster.attach(
    io: File.open(Rails.root.join('app', 'assets', 'images', 'rise_of_the_planet_of_the_apes.png')),
    filename: 'rise_of_the_planet_of_the_apes.png'
)

movie = Movie.create(
    name: 'Cars',
    ranking: 4
)
movie.poster.attach(
    io: File.open(Rails.root.join('app', 'assets', 'images', 'cars.jpg')),
    filename: 'cars.jpg'
)

movie = Movie.create(
    name: 'The Mask of Zorro',
    ranking: 5
)
movie.poster.attach(
    io: File.open(Rails.root.join('app', 'assets', 'images', 'el_zorro.jpg')),
    filename: 'el_zorro.jpg'
)

p "Created #{Movie.count} Movies"