class HomeController < ApplicationController
  def index
    @movies = Movie.all.sort_by{ |movie| movie.ranking }
  end
end